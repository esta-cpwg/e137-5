# ANSI E1.37-5 - 2024, General Purpose Messages for ANSI E1.20 (RDM)

This repository contains supporting files for ANSI E1.37-5, an ANSI standard providing additional parameter messages for use with ANSI E1.20 Remote Device Management (RDM). This standard includes general parameters for use within any type of device, and a Parameter Metadata Language for manufacturer-specific Parameter Messages.

The standard is in active development by the [ESTA](https://tsp.esta.org/tsp/index.html) [Control Protocols Working Group](https://tsp.esta.org/tsp/working_groups/CP/cp.html). To collaborate on the standard and get more context on the contents of this repository, please [join](https://tsp.esta.org/tsp/working_groups/index.html) the CPWG.

## Structure

The project is structured such that each stage of development is contained within a directory. This allows the team to make fixes to the schemas in a previously published revision.

Draft and ratified documents are identified using a number, for example `CP/2014-1049r7` which can be found on the cover page of the document. This number has a direct pairing with a directory within this project.

| Document             | Stage            | Number          |                                             |
| -------------------- | ---------------- | --------------- | --------------------------------------------|
| ANSI E1.37-5 - 2024  | ANSI             | CP/2014-1049r7  | [schemas/1.0.0](/schemas/1.0.0)             |
| Latest Improvements  | Draft            |                 | [schemas/latest](/schemas/latest)           |

## Development

Contribute to the schemas and libraries by opening merge requests against the `main` branch.
